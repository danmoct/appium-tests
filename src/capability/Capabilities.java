package capability;

import org.openqa.selenium.remote.DesiredCapabilities;
import com.google.gson.Gson;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Capabilities {
	
	private DesiredCapabilities caps;
	private Configurations configure;
	
	
	public Capabilities()
	{
		this.caps = new DesiredCapabilities();
		this.configure = new Configurations("Map.json");		
	}
	
	public void setCapabilities()
	{		
		System.out.println(this.configure.deviceName);
		caps.setCapability("deviceName", configure.deviceName);
		caps.setCapability("device", configure.device); //Give Device ID of your mobile phone
		caps.setCapability("platformName", configure.platformName);
		caps.setCapability("platformVersion", configure.platformVersion);		
		caps.setCapability("noReset", configure.noReset);		
	}
	
	public DesiredCapabilities getCapabilities()
	{
		return this.caps;
	}
	
	public void addCapability(String left, String right) 
	{
		this.caps.setCapability(left, right);
		System.out.println("capabilities added with " + left + " " + right);
	}	
	
	
	private class Configurations
	{		
		private String deviceName;
		private String device;
		private String platformName;
		private String platformVersion;
		private String noReset;
		
		private Configurations(String file)
		{
			Gson gson = new Gson();
			Configurations config;
			try (Reader reader = new FileReader(System.getProperty("user.dir")+"/src/DATA/" + file)) {

	            // Convert JSON File to Java Object
	            config = gson.fromJson(reader, Configurations.class);
				System.out.println(config.deviceName);
				this.deviceName = config.deviceName;
				this.device = config.device;
				this.platformName = config.platformName;
				this.platformVersion = config.platformVersion;
				this.noReset = config.noReset;
				
	        } catch (IOException e) {
	            e.printStackTrace();
	        }	
		}		
		
	}	

}
