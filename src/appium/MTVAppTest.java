package appium;

public class MTVAppTest extends Test{
	
	final private String appPackage = "com.google.android.videos";
	final private String appActivity = "com.google.android.videos.activity.LauncherActivity";
	
	public MTVAppTest() throws Exception 
	{
		caps.addCapability("appPackage", appPackage);
		caps.addCapability("appActivity", appActivity);
		initializeDriver();
	}
	
	public void go() throws InterruptedException
	{
		search_click("xpath", "//android.widget.CompoundButton[@content-desc=\"Movies\"]/android.widget.TextView");
		search_click("xpath", "//android.widget.CompoundButton[@content-desc=\"Free\"]/android.widget.TextView");
		//hScroll("com.google.android.videos:id/thumbnail_frame", "Heathers");
		hScroll("Heathers");
		search_click("AccessibilityId", "The Terminator");
		search_click("xpath", "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[3]/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView");
		search_click("id", "com.google.android.videos:id/watchlist_button_image");
		search_click("id", "com.google.android.videos:id/thumb_up_button_image");
		search_click("AccessibilityId", "Play trailer The Terminator");
		search_sendNative("BACK");
		search_sendNative("BACK");
		rotate("PORTRAIT");
		search_click("id", "com.google.android.videos:id/button");
		search_click("xpath", "//android.widget.CompoundButton[@content-desc=\"TV\"]/android.widget.TextView");
		search_click("xpath", "//android.widget.CompoundButton[@content-desc=\"Rent & buy\"]/android.widget.TextView");
		search_click("AccessibilityId", "Friends");
		search_click("id", "com.google.android.videos:id/menu_search");
		search_sendkeys("id", "com.google.android.videos:id/search_box_text_input", "The Flash");
		search_sendNative("Keys.ENTER");
		search_click("id", "com.google.android.videos:id/navigation_button");
		search_sendNative("BACK");
		vScroll("Grey's Anatomy");
		vScroll("SpongeBob SquarePants");
		search_click("AccessibilityId", "Show navigation drawer");
		search_sendNative("APP_SWITCH");
		search_click("AccessibilityId", "Dismiss Google Play Movies & TV.");				
		
	}
	

}
