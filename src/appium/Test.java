package appium;


import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import capability.Capabilities;


public class Test {
	protected String URL;
	protected Capabilities caps;
	protected AppiumDriver<MobileElement> driver;
	//Constructors
	
	
	public Test()
	{		
		this.URL ="http://0.0.0.0:4723/wd/hub";
		caps = new Capabilities();
		caps.setCapabilities();				
	}	
	
	//Methods
	public void go() throws Exception {}	
	
	protected void initializeDriver() throws Exception
	{
		try
		{
			this.driver = new AndroidDriver<MobileElement>(new URL(URL), caps.getCapabilities());
		}
		catch(Exception e)
		{
			System.out.println("Couldn't Load Driver");
		}
	}
	
	protected void search_click (String type, String path) throws InterruptedException
	{
		MobileElement element = search(type,path);
		try
		{		
			element.click();
		}
		catch(Exception e)
		{
			System.out.println("Waiting for Click");
			TimeUnit.SECONDS.sleep(5);
			System.out.println("Trying to click");
			element.click();
			System.out.println("here");
		}
	}
	
	protected String search_getText(String type, String path) throws InterruptedException
	{
		String text = "";
		MobileElement element = search(type,path);
		try
		{		
			text = element.getText();
			System.out.println("got attribute");
		}
		catch(Exception e)
		{
			System.out.println("Waiting for getText");
			TimeUnit.SECONDS.sleep(5);
			System.out.println("Trying to getText");
			text = element.getText();
			System.out.println("here");
		}
		
		return text;		
	}
	
	/*
	protected void search_hover(String type, String path)
    {
       Mobile element = search(type,path);
        Actions action = new Actions(Page.driver);
        action.moveToElement(element).perform();
    }
    */
	
	protected void search_sendkeys(String type, String path, String keys) throws InterruptedException
	{
		MobileElement element = search(type,path);
		try
		{
			element.sendKeys(keys);
		}
		catch(Exception e)
		{
			System.out.println("Waiting to send Text");
			TimeUnit.SECONDS.sleep(5);
			element.sendKeys(keys);
		}
	}
	
	protected void search_sendNative(String keys) throws InterruptedException
	{		
		try
		{
			switch (keys)
			{
				case "Keys.ENTER":
					((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
					break;
					/*
					 * 
					 * 
				case "Keys.ARROW_DOWN":
					element.sendKeys(Keys.ARROW_DOWN);
					break;
					*/
				case "APP_SWITCH":
					((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
					break;
				
				case "BACK":
					TimeUnit.SECONDS.sleep(2);
					((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.BACK));
					break;
			}
		}
		catch(Exception e)
		{
			switch (keys)
			{
				case "Keys.ENTER":
					((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
					break;
					
				case "APP_SWITCH":
					((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
					break;
				
				case "BACK":
					TimeUnit.SECONDS.sleep(2);
					((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.BACK));
					break;
				/*case "Keys.ARROW_DOWN":
					element.sendKeys(Keys.ARROW_DOWN);
					break;
				*/
			}
		}
	}
	
	private MobileElement search(String type, String path) throws InterruptedException
	{
		MobileElement element;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		switch (type)
		{
		case "id":
			element = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(path)));		
			return element;
			
		case "xpath":
			element = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(path)));			
			return element;
			
		case "linkText":
			element = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(path)));	
			System.out.println("found" + path);
			return element;
			
		case "cssSelector":
			element = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(path)));	
			System.out.println("found " + path);
			return element;
			
		case "name":
			element = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(path)));			
			return element;
		case "className":
			element = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(path)));			
			return element;
		case "AccessibilityId":
			try
			{
				element = (MobileElement) driver.findElementByAccessibilityId(path);
			}
			catch(Exception e)
			{
				System.out.println("Waiting");
				TimeUnit.SECONDS.sleep(5);
				element = (MobileElement) driver.findElementByAccessibilityId(path);				
			}
		}		
		return null;			
	}
	
	
	protected void hScroll(String des)
	{
		driver.findElement(MobileBy.AndroidUIAutomator(
				"new UiScrollable(new UiSelector()).setAsHorizontalList().scrollIntoView("
				+ "new UiSelector().descriptionContains(\"" + des + "\"))"));
		/*driver.findElement(MobileBy.AndroidUIAutomator(
				"new UiScrollable(new UiSelector().resourceId(\"" + resId + "\")).setAsHorizontalList().scrollIntoView("
				+ "new UiSelector().descriptionContains(\"" + des + "\"))"));*/
	}
	
	protected void vScroll(String des)
	{
		driver.findElement(MobileBy.AndroidUIAutomator(
				"new UiScrollable(new UiSelector()).scrollIntoView("
				+ "new UiSelector().descriptionContains(\"" + des + "\"))"));
	}

	
	protected void rotate(String orient)
	{
		switch(orient)
		{
			case "PORTRAIT": 
				driver.rotate(ScreenOrientation.PORTRAIT);
		}
		
	}
	
	
	/*
	protected void scroll(String path, int x, int y) 
	{
		JavascriptExecutor js = (JavascriptExecutor) Page.driver;
		js.executeScript(path + ".scrollBy(" + x + "," + y +")");
	}
	
	protected void scroll(int x, int y) 
	{
		JavascriptExecutor js = (JavascriptExecutor) Page.driver;
		js.executeScript("scrollBy(" + x + "," + y +")");
	}
	*/
	
	
		
}
