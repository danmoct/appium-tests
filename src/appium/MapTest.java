package appium;



public class MapTest extends Test {
	
	final private String appPackage = "com.google.android.apps.maps";
	final private String appActivity = "com.google.android.maps.MapsActivity";
	
 
	public MapTest() throws Exception
	{		
		caps.addCapability("appPackage", appPackage);
		caps.addCapability("appActivity", appActivity);
		initializeDriver();
	}
	
	@Override
	public void go() throws Exception{
				
		search_click("className","android.widget.TextView");		
		search_sendkeys("id","com.google.android.apps.maps:id/search_omnibox_edit_text","Dadeland Mall");
		search_click("className","android.widget.TextView");
		search_click("xpath","//android.widget.Button[@content-desc=\"Directions\"]");		
		search_click("xpath", "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout[3]/android.widget.LinearLayout/android.widget.EditText[1]/android.widget.LinearLayout");
		search_sendkeys("id","com.google.android.apps.maps:id/search_omnibox_edit_text", "The Wrigley Building");
		search_sendNative("Keys.ENTER");
		//((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
				
	}

}
