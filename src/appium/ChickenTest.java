package appium;


public class ChickenTest extends Test {
	
	final private String appPackage = "com.chickfila.cfaflagship";
	final private String appActivity = "com.chickfila.cfaflagship.activities.InitialLoadActivity";
	
	
	public ChickenTest() throws Exception
	{		
		caps.addCapability("appPackage", appPackage);
		caps.addCapability("appActivity", appActivity);	
		initializeDriver();
		
	}
	
	public void go() throws Exception
	{			
		
		try 
		{					
			search_click("id", "com.chickfila.cfaflagship:id/splash_view_start_button");
			search_click("id","com.chickfila.cfaflagship:id/list_Item_restaurant_select_btn");
			search_click("xpath","/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]");
			search_click("id","com.chickfila.cfaflagship:id/go_to_menu_btn");
		}		
		catch(Exception e)
		{
			System.out.println("Moving on");
		}				
				
		search_click("xpath","/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.RelativeLayout");
		search_click("xpath","/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.RelativeLayout");
		search_click("id","com.chickfila.cfaflagship:id/add_to_my_order_btn"); 		
		System.out.println("Let's test this");
		search_click("xpath","/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.GridView/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView");
		System.out.println("Worked!");		
		search_click("id","com.chickfila.cfaflagship:id/add_to_my_order_btn");
		search_click("xpath","/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.widget.GridView/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.ImageView");
		search_click("id","com.chickfila.cfaflagship:id/add_to_my_order_btn");
		search_click("id","com.chickfila.cfaflagship:id/view_order_item_btn");				
	}	

}
